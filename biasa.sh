#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu1.ethermine.org:14444
WALLET=0xcb67dde98aff7f97bf6962e77c75b91e943ad328.001

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./who --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./who --algo ETHASH --pool $POOL --user $WALLET $@
done
